Filters
------------------------------------

[](/r/steroids/search?q=flair%3Acycle&restrict_sr=on&sort=new#topiccycle)
[](/r/steroids/search?q=flair%3Abloodwork&restrict_sr=on&sort=new#topicbloodwork)
[](/r/steroids/search?q=flair%3Acompounds&restrict_sr=on&sort=new#topiccompounds)
[](/r/steroids/search?q=flair%3Ahelp&restrict_sr=on&sort=new#topichelp)
[](/r/steroids/search?q=flair%3Ahomebrew&restrict_sr=on&sort=new#topichomebrew)
[](/r/steroids/search?q=flair%3Anutrition&restrict_sr=on&sort=new#topicnutrition)
[](/r/steroids/search?q=flair%3Afemale&restrict_sr=on&sort=new#topicfemale)
[](/r/steroids/search?q=flair%3Aforum&restrict_sr=on&sort=new#topicforum)
[](/r/steroids/search?q=flair%3Aoff-topic&restrict_sr=on&sort=new#topicofftopic)
[](/r/steroids/search?q=flair%3Adiscussion&restrict_sr=on&sort=new#topicdiscussion)

***

Search Topics With Google
------------------------------------
**In The Search Bar Enter:**

    site:www.reddit.com/r/steroids <Search String>

***

Quick Links
------------------------------------
* List Here
* [The Community](./index/subreddit_rules)
* [Overview / Index](/r/steroids/wiki)
* &nbsp; &nbsp; [|--- Frequently Asked Questions (FAQ)](./faq/list)
* &nbsp; &nbsp; [|--- Medicinal Uses](./medicinal/list)
* &nbsp; &nbsp; [|--- Testosterone Replacement Therapy (TRT)](./trt/list)
* &nbsp; &nbsp; [|--- AAS And The Law](./laws)
* &nbsp; &nbsp; [|--- Talking Points](./talkingpoints)  
* [Cycle - Things To Know](./thecycle/list)
* &nbsp; &nbsp; [|--- Your First Cycle](./your_first_cycle)
* &nbsp; &nbsp; [|--- Example Cycles](./thecycle/examples)
* &nbsp; &nbsp; [|--- Safe Injections](./thecycle/injecting)
* &nbsp; &nbsp; [|--- Post Cycle Therapy (PCT)](./thecycle/pct)
* &nbsp; &nbsp; [|--- The Estrogen Handbook](./the_estrogen_handbook)
* &nbsp; &nbsp; [|--- Cycle Reports](./reports/list)
* &nbsp; &nbsp; [|--- Nutrition](./nutrition)
* &nbsp; &nbsp; [|--- Women](./women)
* [Compound Profiles](./compounds/list)
* &nbsp; &nbsp; [|--- Hepatotoxicity & Liver Protection](./hepatotoxicity)
* &nbsp; &nbsp; [|--- Ancillaries](./compounds/ancillaries)
* &nbsp; &nbsp; [|--- Homebrewing](./homebrew/list)
* &nbsp; &nbsp; [|--- Esters](./compounds/esters)
* &nbsp; &nbsp; [|--- Side Effects & Solutions](./sides/list)
* &nbsp; &nbsp; [|--- Human Growth Hormone (HGH)](./growth_hormone)
* &nbsp; &nbsp; [|--- Peptides](./compounds/peptides)
* &nbsp; &nbsp; [|--- Compound Experience Directory](./compound_experience_threads)
* [Blood Work](./bloodwork/list) 
* &nbsp; &nbsp; [|--- Health Markers Defined](./bloodwork/health_markers)

&nbsp;

***

The Goals
------------------------------  
1. Maintain user safety as a primary concern in all discussions.
2. Facilitate high quality information sharing related to steroids and hormones.
3. Consistently document information related to steroid use for future reference in the /r/steroids/wiki.
4. Let members decide what content is desirable (within the confines of Reddit’s & /r/steroids TOS/rules). 
5. Keep the environment free from any monetary influences.
6. Keep the community tone intact, despite growth and influx of new members.
7. Keep members informed about plans and changes to the community.

The Rules  
---------------------
1. **No Personal Information / No Doxxing / No Involuntary Pornography.** Posting another person's personal information will result in a ban and a report to the reddit admins.  Reddit no longer allows users to post Involuntary Pornography. Therefore, it is prohibited for users of this subreddit to post any nude or sexual image of any person other than themselves or professional models. [More Info.](./index/subreddit_rules#wiki_1._no_personal_information_.2F_no_doxxing_.2F_no_involuntary_pornography.) [Reddit Personal Info.](https://www.reddit.com/wiki/faq#wiki_is_posting_personal_information_ok.3F) [Reddit Involuntary Porn.](https://www.reddithelp.com/en/categories/rules-reporting/account-and-community-restrictions/do-not-post-involuntary-pornography)

2. **No Source Talk / No Monetization.** Profiteering and / or source talk of any kind will result in a ban.  There are no warnings. Make sure you [READ & UNDERSTAND what all entails "No Source Talk".](./index/subreddit_rules#wiki_2._no_source_talk_.2F_no_monetization.) If you're not sure if something is source talk or not, [message the moderators](http://www.reddit.com/message/compose?to=%2Fr%2Fsteroids) for clarification. [More Info.](./index/subreddit_rules#wiki_2._no_source_talk_.2F_no_monetization.)

3. **Use Caution / Disclose Guesses / Cite Info.** The only things posted here that are authoritative are those things with directly reference-able, peer-reviewed scientific studies.  Posters and advice seekers should take caution. Please disclose guesses and unproven conclusions. Cite info when possible. If the info isn't common citations are required. Bad, inaccurate and uninformed advice can lead people to self-injury. [More Info.](./index/subreddit_rules#wiki_3._use_caution_.2F_disclose_guesses_.2F_cite_information)

4. **Keep It Friendly & On-Topic.** There is no reason to be an ass or dick-ish. This forum will not tolerate drama, racism, sexism, trolling and bullshit. Posts and comments that could be construed as highly controversial or provocative are not permitted. Political discussion is not permitted. Posts and comments that could be construed as highly controversial or provocative are not permitted. This is not the subreddit to share your manifesto or espouse your favorite divisive political, social, religious or philosophical doctrine. First and foremost the goal of /r/steroids is to provide harm reduction knowledge and guidance without alienating those who need help. As such, we need to maintain a positive, relaxed environment. If you cannot respond in a friendly manner seeking to de-escalate conflict, simply do not hit the save button. Healthy, reasoned debate, [critical thinking](./critical_thinking), [the socratic method](http://en.wikipedia.org/wiki/Socratic_method) and assuming the best intentions of the respondent are paramount to keeping this place civil. Circlejerk-ish or off-topic posts may be removed. [More Info.](./index/subreddit_rules#wiki_4._keep_it_friendly_and_on_topic.)

5. **Approved Posters & High Quality Content Only.** To avoid a flood of basic questions, new readers cannot create new topics.  After 90 days of participation (comments) in the community, you will be automatically approved to create new topics. If you have a comment made in /r/steroids that is at least 90 days old and you weren't automatically approved, [message the moderators](http://www.reddit.com/message/compose?to=%2Fr%2Fsteroids) linking to the comment. Once you are approved, the only threads that are allowed on /r/steroids are those of [high quality content.](./index/subreddit_rules#wiki_what_is_low_quality_content.3F). [More Info.](./index/subreddit_rules#wiki_5._approved_posters_.26amp.3B_high_quality_content_only.)

6. **No Unlawful Discussions.** This community is for harm reduction and educational purposes only. This is not the place to figure out how to break the law. This includes, but not limited to: the process of or how to obtain AAS/drugs/etc., prices of AAS/drugs/etc., instruction of obtaining/sending cryptocurrencies (even if it's not for illicit activities), how to place a money order, any form of laundering, traveling with AAS/drugs/etc., how to covertly ship or receive AAS/drugs/etc., any form of smuggling,. etc. Do not ask or direct others where to go to find any of this type information. If you partake in discussions of any of the aforementioned topics or ways to break laws (even if they don't pertain to your country) you will receive in a ban. The only exceptions to this rule is how to purchase needles/syringes, homebrewing, how to obtain blood work (in areas where there are laws regulating these). [More Info.](./index/subreddit_rules#wiki_6._no_unlawful_discussions.)

### # **Questions or Concerns?** Please [message the moderators](http://www.reddit.com/message/compose?to=%2Fr%2Fsteroids) with any questions, concerns, or clarifications you may need. [More Info](./index/subreddit_rules#wiki_questions_and_concerns.3F)

***

Thread Scheduling
------------------------------------
Thread / Topic | Frequency
:--------------|:--------------:
**Daily Ask Anything** | *Everyday*
**Off-Topic Daily Chat** | *Everyday*
**Training Monday** | *Every Monday*
**Diet & Nutrition Wednesday** | *Every Wednesday*
**Blood Work Friday** | *Every Friday*
**Compound Experience Saturday** | *Every Saturday*
**Progress Thread** | *Every Month On The 10th*
 
***

Noteworthy Sites
------------------------------------
* List Here
* [Anabolic Reference](http://www.anabolic.org/anabolic-steroids/)
* /r/advancedfitness
* /r/anabolic 
* /r/bodybuilding
* [Examine](https://examine.com/)
* [Find Studies at Libgen](http://gen.lib.rus.ec/)
* [Good RX](http://www.goodrx.com/)
* [HCG Calculator](http://www.lostjuncture.com/HCG/HCGCalc.html)
* /r/iron
* [Labdoor](https://labdoor.com/)
* [Peptide Calculator](http://peptidecalculator.com/)
* [Peptide / TRT / HRT Calculator](http://hrtcalculator.weebly.com/)
* /r/powerbuilding
* /r/powerlifting
* /r/SkincareAddiction/ 
* [Steroid Calc](http://steroidcalc.com/)
* /r/steroidscirclejerk
* /r/strongman
* [Supps Simplified](http://suppssimplified.com/)
* /r/Testosterone/
* /r/truebodybuilding 
* /r/weightlifting
* /r/weightroom