# Trestolone

aka Trestolone Acetate, 7*α*\-**methyl**\-19-**nortestosterone** (MENT)

**Anabolic/Androgenic Ratio:** 2300:650  

**CAS**\#: 3764-87-2

**Chemical Structure:** 7α-Methylestr-4-en-17β-ol-3-one

**Molecular Weight:**  288.431 g/mol g·mol(sup)-1

##  Description

**Trestolone** (**MENT**) is an experimental androgen/anabolic steroid (AAS) and progestogen medication under development by the USG and university partners for potential use as a form of hormonal birth control for men and in androgen replacement therapy for low testosterone levels in men, but has never been marketed for medical use. The compound is an extremely potent 19-nor derivative oft-described as a cross between tren and deca—but without any of the negative sides of either. Perhaps more so than any other compound, MENT is highly capable of eliciting serious gains in mass and strength over a relatively short period of time.

Trestolone delivers 10x more myotropic effect of testosterone. The *myotropic effect* is the effect of building muscle, making trestolone more effective in building muscle than any other commercial steroid in existence. In fact, trestolone outperforms trenbolone by a whopping 250% in this area. It's oft-described as “test on steroids” or “supercharged test.”

##  Metabolic Equivalence to Testosterone 

Trestolone is the *only steroid in production today* that is capable of sustaining normal male physiology in the complete absence of testosterone, including sexual functioning.

Trestolone has an anabolic/androgenic rating of 2300:650.  In rats, it was found to be roughly ten times as myotropic as methyltestosterone. 

The psychological effects MENT conveys include a positive sense of confidence, euthymia and well-being that are noted to be more heightened even than those conveyed by testosterone or DHT derivatives Proviron and Masteron.

##  Clinical Studies

In a 1992 study in rats investigating the pharmacology of MENT, researchers found the anabolic potency of MENT to be 10X greater than that of testosterone, while also being 12X more suppressive on HTPA. This means that if your average male produces 4-7mg of testosterone daily to keep muscle mass and sexual function, only 400-700mcg of MENT is needed to produce the same results.

In this same study, MENT acetate was used to determine the rate of hydrolysis of the acetate ester and how quickly elevated MENT plasma levels were reached. The half-life is also VERY short, calculated to be only 40 min for the unesterified compound (not the acetate, which should extend the half-life slightly). 

A comprehensive human study conducted on male subjects compared the effects of MENT to testosterone, both accompanied with the progestin etonogestrel, on spermatogenesis, as well as sex drive and safety biomarkers associated with androgen use. This was a robust study, containing 29 subjects tested for 48 weeks.^(8)

Participants either received two implant pellets each containing 135mg of MENT acetate calculated to release 400mcg daily, or three 600mg testosterone pellets, with one given every 12 weeks. Both groups received 68mg of etonogestrel. Both groups experienced significant reduction in sperm production, with 80% of both groups going from an average of 55 x 10^6 mL to just 1 x 10^6 mL after 12 weeks of supplementation.^(8)

Recovery from MENT was *much more rapid* than from testosterone. The recovery period was 16 weeks for the MENT group, in which *semen concentration increased* to over 20 x 10^6 mL—whereas the testosterone group *still had azoospermia* until after 28 weeks.^(8)

##  Dosage Guidelines 

Weekly dosage is low. In clinical studies, the injectable pellets used released between 400-700 micrograms per day as replacement for natural testosterone production values between 4-7 milligrams per day—ten times the strength of testosterone.

A common dose for AAS users is 10 mg per day, or 70 mg per week. Megablast dosages are anywhere from 25-50 mg per day, or 175-350 mg per week, even on up to 100 mg/day. Cycle duration is oft-limited to 6-8 weeks due to extremely rapid physique changes. 

##  References

1.  [Wikipedia: Trestolone](https://en.wikipedia.org/wiki/Trestolone)  
 
2. Reddit [Compound Experience Thread: Trestolone](https://www.reddit.com/r/steroids/comments/86t5st/compound_experience_saturday_trestolone_trest_ment/)  

3.  Anderson, Richard A., et al. “[Evidence for tissue selectivity of the synthetic androgen 7*α*-methyl-19-nortestosterone in hypogonadal men](https://www.ncbi.nlm.nih.gov/pubmed/12788888).” *The Journal of Clinical Endocrinology & Metabolism* **88.6** (2003): 2784-2793.

4.  Sundaram, Kalyan, Narender Kumar, and C. Wayne Bardin. “[7*α*-methyl-19-nortestosterone (MENT): the optimal androgen for male contraception](https://www.ncbi.nlm.nih.gov/pubmed/8489761).” *Annals of Medicine* **25.2** (1993): 199-205.

5.  Liu, Aijun, Kathryn E. Carlson, and John A. Katzenellenbogen. “[Synthesis of high-affinity fluorine-substituted ligands for the androgen receptor. Potential agents for imaging prostatic cancer by positron emission tomography](https://pubs.acs.org/doi/abs/10.1021/jm00089a024).” *Journal of Medicinal Chemistry* **35.11** (1992): 2113-2129.

6.  Kumar, Narender, et al. “[Pharmacokinetics of 7*α*-methyl-19-nortestosterone in Men and Cynomolgus Monkeys](https://www.ncbi.nlm.nih.gov/pubmed/9283946).” *Journal of Andrology* **18.4** (1997): 352-358.

7.  Walton, Melanie J., et al. "[7*α*-methyl-19-nortestosterone (MENT) vs Testosterone in Combination With Etonogestrel Implants for Spermatogenic Suppression in Healthy Men](https://www.ncbi.nlm.nih.gov/pubmed/17460095)." *Journal of Andrology* **28.5** (2007): 679-688.