# Trenavar

**Chemical structure:** Estra-4,9,11-triene-3,17-dione (C_18 H_20 O_2)

**Molecular weight of base:** 268.350189 (isotopic average)

**Active Life:** unknown, presumably short like most orals

**Anabolic/Androgenic Ratio:** unknown, presumably comparable to trenbolone

##  Use/Dosing
[Trenbolone](http://www.reddit.com./trenbolone) is a well-known, extremely potent injectable steroid; it is a strong androgen with no estrogenic activity that causes great strength gains, increase in lean muscle mass, and fat loss, though many users report harsh side effects (see [the wiki page](http://www.reddit.com./trenbolone) on trenbolone for more detail).  Trenavar is a prohormone that promises to convert directly to trenbolone in the body.  

Conversion to trenbolone should be high, so effects should be identical to the injectable form - with the exception of the famed "tren cough". Whatever the explanation for "tren cough" (and many have been suggested), since it's a reaction to the sudden parenteral introduction of some compound directly into the body, it's highly unlikely that any orally administered compound will have the same effect. Trenbolone is one of the strongest injectable steroids on the market, so effects experienced from Trenavar can be expected to be largely the same - huge strength and size increases, accelerated fat loss, and enhanced vascularity.

Usually its recommended to start between 60-90mg/day working your way up until the sides are too much.  Anecdotal evidence for higher doses (up to 250mg/day) exists, see links below.   Average cycles are typically 4-6 weeks and should be run with a test base..

##  Side Effects/Risks
Blood pressure is likely to be dose-dependently elevated to a significant degree, cholesterol levels and liver function markers are likely to be adversely affected, though to what extent remains to be seen. Commonly reported trenbolone sides include night-sweats, mood swings, androgenic hair loss and/or growth, temporary loss of libido, as well as the suppression of endogenous testosterone production, and it would be sensible to assume that these may also result from use of Trenavar.

While trenavar converts to trenbolone in the body, it seems that the infamous "tren cough" is NOT a side effect of trenavar.  Whatever the explanation for "tren cough" (and many have been suggested), since it's a reaction to the sudden parenteral introduction of some compound directly into the body, it's highly unlikely that any orally administered compound will have the same effect. 

##  Related Posts
http://www.reddit.com/r/steroids/comments/2qbg92/sotd_trenavar_the_ultimate_stocking_stuffer/

[/u/JohnDenversCoPilot's experience](http://www.reddit.com/r/steroids/comments/2qbg92/sotd_trenavar_the_ultimate_stocking_stuffer/cn4lm30)

##  References