# Table Of Contents

### [Format Key [Click HERE]]( )

# I.) [The Community](./index/subreddit_rules)

# II.) [Overview / Introduction]( )

##  [Frequently Asked Questions (FAQ)](./faq/list)

# III.) [Medicinal Uses](./medicinal/list)

##  [Testosterone Replacement Therapy (TRT)](./trt/list)

# IV.) Essential Cycle / Blast & Cruise Reading

##  [Safe Injections](./thecycle/injecting)

##  [Post Cycle Therapy (PCT)]( )

##  [The Estrogen Handbook](./the_estrogen_handbook)


# IV.) Cycling / Blasting & Cruising (B&C)-  [The Cycle - Things To Know](./thecycle/list)

##  [Your First Cycle](./your_first_cycle)

##  [Cycle Examples](./thecycle/examples)

# VI.) [Anabolic Steroids And The Law](./laws)

# VII.) [Talking Points](./talkingpoints)






# XIII.) [Nutrition](./nutrition)

#