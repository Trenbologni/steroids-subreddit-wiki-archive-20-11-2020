# Hair Loss [Under Construction]

Pretty much any strong androgen or DHT compound will greatly increase risk of hairloss.

EQ has a chance to, however finasteride and durasteride will prevent EQ from becoming DHB. EQ becomes DHB through contact with the 5-alpha reductase enzyme, same way test becomes DHT. DHB is not known for being a very active androgen.

The below table applies to any ester.

	Compound | Promotes Loss? | To help prevent | Notes
:--------------|:--------------:|:-------------:|:-------------:|:-------------
[Anadrol 50 (Oxymetholone)](./anadrol) | Yes | Don't use |   
[Anavar (Oxandrolone)](./anavar) | Yes | finastride / dutastride  | Very mild  
Androgel (Testosterone) | Yes | finastride / dutastride  |   
[Deca-Durabolin (Nandrolone Decanoate)](./deca_durabolin) | | |   
[Dianabol (Methandrostenolone)](./dianabol) | | | 
Durabolin (NPP) | | |   
[Equipoise (Boldenone Undecylenate)](./equipoise) | | |    
Epistane (Methylepitiostanol)| | |   
[Halotestin (Fluoxymesterone)](./halotestin) | | |   
[Masteron (Drostanolone Propionate)](./masteron) | No | ----- |  
[Methylstenbolone](http://www.reddit.com./compounds/methylstenbolone) | | | 
Nandrolone | | | 
[Oral Turinabol](./turinabol) | | | 
[Primobolan (Methenolone Acetate)](./primobolan) | | |
[Proviron (Mesterolone)](./proviron) | | |
[Superdrol (Methyldrostanolone)](./gear/methasterone) | Yes | Don't use | 
[Sustanon 100 & 250](./sustanon_250) | Yes | finastride / dutastride  |   
Testosterone | Yes | finastride / dutastride  |   
[Trenbolone Acetate/Enanthate & Blends](./trenbolone) | No | ----- |
[Winstrol (Stanozolol)](./winstrol) | Yes | Don't use |  


